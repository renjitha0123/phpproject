<?php include('server.php') 
?>
<?php echo $_SESSION['usernmae']; ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Registration system </title>
    <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
  	  <h2>Register</h2>
    </div>
    <form method="post" action="register.php">
  	  <?php include('errors.php'); ?>
      <div class="input-group">
        <label>FirstName</label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>">
      </div>
      <div class="input-group">
        <label>LastName</label>
        <input type="text" name="lastname" value="<?php echo $lastname; ?>">
      </div>
  	  <div class="input-group">
  	    <label>Username</label>
  	    <input type="text" name="usernmae" value="<?php echo $usernmae; ?>">
  	  </div>
  	  <div class="input-group">
  	    <label>Email</label>
  	    <input type="email" name="email" value="<?php echo $email; ?>">
  	  </div>
  	  <div class="input-group">
  	    <label>Password</label>
  	    <input type="password" name="password">
  	  </div>
  	  <div class="input-group">
  	    <button type="submit" class="btn" name="reg_user">Register</button>
  	  </div>
  	  <p>
  		  Already a member? <a href="login.php">Sign in</a>
  	  </p>
    </form>
  </body>
</html>
