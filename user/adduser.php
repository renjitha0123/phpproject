<?php include('server.php') 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Add User</title>
    <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
  	  <h2>ADD USER</h2>
    </div>
    <form method="post" action="adduser.php" enctype="multipart/form-data">
  	  <?php include('errors.php'); ?>
      <div class="input-group">
        <label>FirstName</label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>">
      </div>
      <div class="input-group">
        <label>LastName</label>
        <input type="text" name="lastname" value="<?php echo $lastname; ?>">
      </div>
  	  <div class="input-group">
  	    <label>Username</label>
  	    <input type="text" name="username" value="<?php echo $username; ?>">
  	  </div>
  	  <div class="input-group">
  	    <label>Email</label>
  	    <input type="email" name="email" value="<?php echo $email; ?>">
  	  </div>
      <div class="input-group">
        <label>Phone Number</label>
        <input type="number" name="phone" value="<?php echo $phone; ?>">
      </div>
      <div class="input-group">
        <label>User Role</label>
        <input type="text" name="role" value="<?php echo $role; ?>">
      </div>
      <div class="input">
        <label>Image Upload</label>
        <input type="file" name="fileToUpload" id="fileToUpload">
      </div>
  	  <div class="input-group">
  	    <label>Password</label>
  	    <input type="password" name="password">
  	  </div>
  	  <div class="input-group">
  	    <button type="submit" class="btn" name="reg_user">Add</button>
  	  </div>
    </form>
  </body>
</html>