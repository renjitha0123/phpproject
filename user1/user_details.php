<?php  
session_start();
$servername="localhost";
$user="root";
$pass="root";
$dbname="product";
$conn = new mysqli($servername,$user,$pass,$dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>User Details</title>
    <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
      <h2>User Details</h2>
    </div>
    <?php  if (isset($_SESSION['username'])) : ?>
     
      <div class="input-group">
        <label>Username</label>
        <input type="text" name="username" value="<?php echo $_SESSION['username']; ?>">
      </div>
        <label>FirstName</label>
        <input type="text" name="firstname" value="<?php echo $_SESSION['firstname']; ?>">
      </div>
      <div class="input-group">
        <label>LastName</label>
        <input type="text" name="lastname" value="<?php echo $_SESSION['lastname']; ?>">
      </div>
      <div class="input-group">
        <label>Email</label>
        <input type="email" name="email" value="<?php echo $_SESSION['email']; ?>">
      </div>
      <div class="input-group">
        <label>Phone Number</label>
        <input type="number" name="phone" value="<?php echo $_SESSION['phone']; ?>">
      </div>
    <?php endif ?>
  </body>
</html>