<?php include('server.php') 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Add Product</title>
    <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
  	  <h2>ADD PRODUCTS</h2>
    </div>
    <form method="post" action="addproduct.php" enctype="multipart/form-data">
  	  <?php include('errors.php'); ?>
        <input type="hidden" name="id" value="<?php echo $prodID; ?>">
      <div class="input-group">
        <label>Name of the Product</label>
        <input type="text" name="name_product" value="<?php echo $name; ?>">
      </div>
      <div class="input-group">
        <label>Description Of the Product</label>
        <input type="text" name="description_product" value="<?php echo $description_product; ?>">
      </div>
      <div class="input">
        <label>Image Upload</label>
        <input type="file" name="fileToUpload" id="fileToUpload" />
      </div>
  	  <div class="input-group">
  	    <label>Cateory</label>
        <select name="cateory" value="<?php echo $cateory; ?>">
          <option value="<?php echo $cateory_product;?>"><?php echo $cateory_product;?>
          </option>
	      </select>   
  	  </div>
      <?php if ($update == true): ?>
      <button class="btn" type="submit" name="update" style="background: #556B2F;" >update</button>
      <?php else: ?>
      <button class="btn" type="submit" name="prod_user" >Save</button>
      <?php endif ?>
    </form>
  </body>
</html>