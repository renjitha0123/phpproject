<?php 
session_start();
$servername="localhost";
$user="root";
$pass="root";
$dbname="product";
$conn = new mysqli($servername,$user,$pass,$dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>User Page</title>
  </head>

  <body>
    <div class="container-fluid">
      <div id="div">
        <div class="header">
          <h2>Purchased Products</h2>
        </div>
        <div>
          <table width="100%" border="1">
            <thead>
              <tr>
                <th><strong>ProductID</strong></th>
                <th><strong>ProductName</strong></th>
               <th><strong>Description</strong></th>
               <th><strong>Cateory</strong></th>
              </tr>
            </thead>
            <tbody>
              <?php
              $count=1;
              $sel_query="Select * from buy";
              $result = mysqli_query($conn,$sel_query);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) { ?>
                  <tr>
                    <td align="center"><?php echo $row["prodID"]; ?></td>
                    <td align="center"><?php echo $row["name_product"]; ?></td>
                     <td align="center"><?php echo $row["description"]; ?></td>
                      <td align="center"><?php echo $row["cateory"]; ?></td>
                  </tr>
                <?php $count++; 
                } 
              }?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>