<?php 
session_start();
$servername="localhost";
$user="root";
$pass="root";
$dbname="product";
$conn = new mysqli($servername,$user,$pass,$dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>User Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script>
      window.onload = function () {
        document.getElementById("div").style.visibility = "hidden";
        document.getElementById("li").addEventListener("click", function () {
          var currentstate = document.getElementById("div").style.visibility;
          var newstate = currentstate == "hidden" ? "visible" : "hidden"
          document.getElementById("div").style.visibility = newstate;
        });
      }  
    </script>
    <style>
      .row.content {
        height: 1500px
      }
      .sidenav {
        background-color: #f1f1f1;
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-3 sidenav">
          <h4>User Page</h4>
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="purchased_product.php">PURCHASED PRODUCT</a></li>
            <?php
              $sel_query="Select * from user";
                $result = mysqli_query($conn,$sel_query);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    $userID = $row['userID'];
                    $username = $row['username'];
                    $firstname = $row['firstname'];
                    $lastname = $row['lastname']; 
                    ?>
                    <li><a href="user_details.php?view1=<?php echo  $_SESSION['username']; ?>" class="view_btn">USER DETAILS</a></li>
                  <?php  
                  } 
                $_SESSION['userID'] = $userID;
                $_SESSION['username'] = $username;
                $_SESSION['firstname'] = $firstname;
                $_SESSION['lastname'] = $lastname; 
                }?>
            <li id="li"><a href="#">PRODUCT LISTING</a></li>         
          </ul><br>
        </div>
      <div class="col-sm-9">
          <div id="div">
            <div class="header">
              <h2>Product List</h2>
            </div>
            <div>
              <table width="100%" border="1" style="border-collapse:collapse;">
                <thead>
                  <tr>
                    <th><strong>ProductID</strong></th>
                    <th><strong>ProductName</strong></th>
                    <th><strong>View</strong></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $count=1;
                  $sel_query="Select prodID,name_product from products";
                  $result = mysqli_query($conn,$sel_query);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) { ?>
                      <tr>
                        <td align="center"><?php echo $row["prodID"]; ?></td>
                        <td align="center"><?php echo $row["name_product"]; ?></td>
                        <td align="center">
                         <a href="prod_view.php?view=<?php echo $row["prodID"]; ?>" class="view_btn">View</a>
                        </td>
                      </tr>
                    <?php $count++; 
                    } 
                  }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
